# T3nhance v1.0.1 [![Chrome Web Store](https://img.shields.io/chrome-web-store/v/abehcgeppbcjfckdoaccjfengclahbem.svg) ![Chrome Web Store](https://img.shields.io/chrome-web-store/stars/abehcgeppbcjfckdoaccjfengclahbem.svg) ![Chrome Web Store](https://img.shields.io/chrome-web-store/users/abehcgeppbcjfckdoaccjfengclahbem.svg)](https://chrome.google.com/webstore/detail/abehcgeppbcjfckdoaccjfengclahbem/) ![TYPO3 support](https://img.shields.io/badge/-4--9_LTS-green.svg?logo=typo3&colorA=555) <img src="src/icon.svg" align="right" />
> Utilities for TYPO3 CMS backend editors

This extension is meant to support editors in the backend of [TYPO3 CMS](https://typo3.org/) websites.

## Hotkeys
*T3nhance* detects the use of well-known hotkeys to provide some additional functionality.

Hotkey           | Action
:---:            | ---
`F5` or `CTRL+R` | Reloads the content frame to the right instead of reloading the whole backend
`CTRL+S`         | Attempts to save the currently edited page or content instead of opening the browser's "Save As" dialog
`ESC`            | Attempts to close the currently edited page or content.

## Fixes
* Starting in TYPO3 9, the *TypoScript Object Browser* backend module shows tooltips for the values of "UN-substituted
constants" (if selected), although the tooltips have fixed widths and trim long values. *T3nhance* fixes this.

## Supported versions
This extension was tested and confirmed to work with TYPO3 versions ranging **from v4 LTS to v9 LTS**.

---

<small>(may include colored eggs)</small>
