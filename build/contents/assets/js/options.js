document.addEventListener('DOMContentLoaded', () => {
	const eggsOption = document.querySelector('#eggs')

	eggsOption.addEventListener('change', event => {
		chrome.storage.sync.set({
			eggs: event.target.checked
		}, () => {})
	})

	chrome.storage.sync.get(['eggs'], result => {
		eggsOption.checked = result.eggs
	})
})
