(() => {
	return class t3nhance {
		static init() {
			if(this.root) chrome.runtime.sendMessage({ badgeText: this.typo3Version }, () => {})

			this.fixTypoScriptObjectBrowserTooltips()
			this.addAwesomeHotKeys()
		}
		static get root() {
			return !!this.iframe
		}

		static get iframe() {
			return (
				/* v9+ */          document.querySelector('#typo3-contentIframe')
				/* below v9 */  || document.querySelector('iframe[name="content"]')
			)
		}

		static get typo3Version() {
			let version = '?'

			const versionElement =
				/* v8+ */      document.querySelector('.topbar-header-site-version')
				/* v7 */    || document.querySelector('.typo3-topbar-site-name')
				/* v6 */    || document.querySelector('title')

			if(versionElement) {
				const match = versionElement.textContent.match(/(\d+\.\d+\.\d+)]?$/)
				if(match) version = match[1]
			}

			return version
		}

		static refresh(event) {
			event.preventDefault()

			if(this.root)   this.iframe.src = this.iframe['src'] // avoiding IDE warnings for "x = x"
			else            location.reload()
		}

		static saveDoc(event) {
			if(this.root) return
			event.preventDefault()

			const saveButton =
				/* v7+ */      document.querySelector('button[name="_savedok"]')
				/* v6 */    || document.querySelector('input[type="image"][name="_savedok"]')

			if(saveButton) saveButton.click()
		}

		static closeDoc(event) {
			if(this.root) return
			event.preventDefault()

			const closeButton =
				/* v7+ */      document.querySelector('.t3js-editform-close')
				/* v6 */    || document.querySelector('.typo3-docheader-buttons a[onclick*="closeDoc"]')
				/* v4 */    || document.querySelector('#typo3-docheader a[onclick*="closeDoc"]')

			if(closeButton) closeButton.click()
		}

		/* Extend template browser tooltips to prevent cropping of long labels */
		static fixTypoScriptObjectBrowserTooltips() {
			if(!this.root && document.querySelector('.tsob-menu')) {
				const style = document.createElement('style')
				style.textContent = '.tooltip-inner { max-width: none !important; }'
				document.body.appendChild(style)
			}
		}

		/* Adds awesome hot keys */
		static addAwesomeHotKeys() {
			document.querySelector('body').addEventListener('keydown', event => {
				const refresh =     event.keyCode === 116 || (event.keyCode === 82 && event.ctrlKey)
				const saveDoc =     event.keyCode === 83 && event.ctrlKey
				const closeDoc =    event.keyCode === 27

				if(refresh)         t3nhance.refresh(event)
				else if(saveDoc)    t3nhance.saveDoc(event)
				else if(closeDoc)   t3nhance.closeDoc(event)
			})
		}
	}.init()
})()
