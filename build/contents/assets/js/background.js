chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	if(request.badgeText) {
		chrome.tabs.get(sender.tab.id, tab => {
			if(chrome.runtime.lastError) return

			if(tab.index >= 0) chrome.browserAction.setBadgeText({tabId: tab.id, text: request.badgeText})
			else {
				chrome.webNavigation.onCommitted.addListener(function update(details) {
					if (details.tabId === sender.tab.id) {
						chrome.browserAction.setBadgeText({tabId: details.tabId, text: request.badgeText})
						chrome.webNavigation.onCommitted.removeListener(update)
					}
				})
			}
		})
	}

	if(request === 'eggs') {
		chrome.storage.sync.get(['eggs'], result => {
			sendResponse(result.eggs)
		})
		return true
	}

	sendResponse()
})
