(() => {
	/* Make TYPO3 exceptions more fun to look at */
	const exceptionWrapper = document.querySelector('.exception-message-wrapper')
	if(exceptionWrapper) {
		chrome.runtime.sendMessage('eggs', eggs => {
			if(!eggs) return

			const style = document.createElement('style')
			style.textContent = `
				.exception-message { display: inline-block; }
				
				.exception-flipper {
				    display: inline-block;
				    font-size: 12px;
				    transform: rotateZ(180deg);
				    margin-left: -1rem;
				}
				.exception-illustration { animation: rotation .000944s forwards infinite linear; }
				
				@keyframes rotation {
				    from { transform: rotateZ(0); }
				    to { transform: rotateZ(360deg); }
				}
			`.trim()

			exceptionWrapper.appendChild(style)

			document.querySelector('.exception-message').textContent = '(ノ°Д°)ノ︵'

			const wrap = document.createElement('div')
			wrap.classList.add('exception-flipper')
			wrap.appendChild(exceptionWrapper.querySelector('.exception-illustration'))

			exceptionWrapper.appendChild(wrap)
		})
	}
})()
